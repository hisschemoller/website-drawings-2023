import { getDrawings } from './data';
import { monthNames } from './util';

/**
 * 
 */
export function setupDrawingsInfiniteScroll() {
  const containerEl = document.getElementById('infinite-scroll');
  if (containerEl) {

    // intro tekst
    containerEl.innerHTML = `<div class="infinite-scroll-intro">
      <p>Tekeningen met pen en potlood, meestal ter plekke buiten getekend.</p>
    </div>`;

    const pageSize = 10;
    const allDrawings = [...getDrawings()].sort((a, b) => {
      if (a.date < b.date) {
        return 1;
      }
      if (a.date > b.date) {
        return -1;
      }
      return 0;
    });
    let pageIndex = 0;
    let isAtEnd = false;

    const getPage = () => {
      const startIndex = pageIndex * pageSize;
      const drawings = allDrawings.slice(startIndex, startIndex + pageSize);

      if (drawings.length < pageSize) {
        isAtEnd = true;
      }

      drawings.map((drawing) => {
        const { date, id, image_file_small, title } = drawing;
        const d = new Date(date);
        const el = document.createElement('div');
        el.classList.add('infinite-scroll-drawing');
        el.innerHTML = `<img src="/images/drawings/${image_file_small}" loading="lazy" alt="${title}" data-id="${id}">
        <p>${title}<br>${d.getDate()} ${monthNames[d.getMonth()]} ${d.getFullYear()}</p>`;
        containerEl.appendChild(el);
      });
    };

    document.onscroll = () => {
      if (!isAtEnd && Math.ceil(containerEl.clientHeight + containerEl.scrollTop) >= containerEl.scrollHeight) {
        pageIndex++;
        getPage();
      }
    };

    getPage();
  }
}
