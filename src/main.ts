import { setupSlider } from './slider';
import { loadDrawingsJson } from './data';
import { addMapClusterLayer, addMapPopupOverlay, setupMap } from './map';
import { setupMapClusters } from './mapCluster';
import { setupOverlay } from './overlay';
import { setupThumbnailCarousel } from './carousel';
import { setupDrawingsInfiniteScroll } from './infinitescroll';

(async function setup() {
  await loadDrawingsJson();

  if (window.innerWidth < 768) {
    setupDrawingsInfiniteScroll();
  } else {
    setupMap();
    setupMapClusters();
    addMapClusterLayer();
    addMapPopupOverlay();
    setupSlider();
    setupOverlay();
    setupThumbnailCarousel();
  }
})();
