# Website Drawings 2023

The Website Drawings app rewritten without Vue to avoid problems.

# About

On desktop all drawing locations are shown on an OpenLayers map as clustered markers, and all
drawing thumbnails as a carousel at the bottom of the page.

On phone screens this is too cumbersome use so an infinite scroll list is shown instead.


## Documentation

* Openlayers
  * https://openlayers.org/
* Popup
  * https://openlayers.org/en/latest/examples/popup.html
* Clustered Features
  * https://openlayers.org/en/latest/examples/cluster.html  
* Splide
  * https://splidejs.com/
* Infinite scroll
  * https://infinite-scroll.com/


## Run local

* Build _bundle.js_ with `ENV = 'dev'` in _data.ts_
* Run `yarn build` every time to build, there's no watcher.
* Run `yarn dev` to start the server.
* Open http://localhost:3021/
* MAMP's http://website-drawings-2023.localdev/ not used anymore.


## Deploy as a WordPress plugin

* Add CSS and JS paths, HTML string to _drawings-app.php_
* Build _bundle.js_ with `ENV = 'prod'` in _data.ts_
* _drawings.json_ komt uit de lokale PhpMyAdmin van de __website-drawings-editor__ app.


## Plugin file structure

```
drawings-app
+-- drawings-app.php
+-- dist
    +-- css
    |   +-- splide.min.css
    |   +-- style.css
    +-- js
    |   +-- bundle.js
    +-- json
        +-- drawings.json
```
